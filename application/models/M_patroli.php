<?php

class M_patroli extends CI_Model
{
	public function simpanlokasipatroli($data)
	{
		return $this->db->insert('lokasi_kartu_patroli', $data);
	}

	public function cekeksisuid($uid)
	{
		$this->db->select('count(uid) as jmluid');
		$this->db->from('lokasi_kartu_patroli');
		$this->db->where('uid', $uid);
		return $this->db->get();
	}

	public function getalldatalokasi()
	{
		$this->db->select('*');
		$this->db->from('lokasi_kartu_patroli');
		$this->db->order_by('id_lokasi_patroli', 'ASC');
		return $this->db->get();
	}

	public function getalldatalokasibynama($nama)
	{
		$this->db->select('*');
		$this->db->from('lokasi_kartu_patroli');
		$this->db->like('nama_lokasi', $nama);
		return $this->db->get();
	}

	public function getalldatalokasibyidlokasi($id)
	{
		$this->db->select('*');
		$this->db->from('lokasi_kartu_patroli');
		$this->db->where('id_lokasi_patroli', $id);
		return $this->db->get();
	}

	public function updatenamalokasipatroli($data, $id)
	{
		return $this->db->update('lokasi_kartu_patroli', $data, array('id_lokasi_patroli' => $id));
	}

	public function hapusnamalokasipatroli($id)
	{
		$this->db->delete('lokasi_kartu_patroli', array('id_lokasi_patroli' => $id));
	}

	public function getdatalokasipatrolibyuid($id)
	{
		$this->db->select('*');
		$this->db->from('lokasi_kartu_patroli');
		$this->db->where('uid', $id);
		return $this->db->get();
	}

	public function getallkaryawan()
	{
		$this->db->select('a.nama, b.nama_bagian, c.nama_level');
		$this->db->from('karyawan a');
		$this->db->join('bagian_karyawan b', 'a.id_bagian=b.id_bagian', 'INNER');
		$this->db->join('level_karyawan c', 'c.id_level=a.id_level', 'INNER');
		return $this->db->get();
	}
}
