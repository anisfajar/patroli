<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Patroli extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        header('Content-Type: application/json');
        $this->load->database();
        $this->load->model('m_patroli');
    }

    public function index_get()
    {
        # code...
    }

    public function savelokasi_post()
    {
        $uid = $this->post('uid');
        $nama = $this->post('nama');
        $gps = $this->post('gps');

        if ($uid == "") {
            $result = [
                'status'    => FALSE,
                'pesan'   => 'Maaf, UID kartu harus diisi terlebih dahulu'
            ];
            echo json_encode($result);
        } elseif ($nama == "") {
            $result = [
                'status'    => FALSE,
                'pesan'   => 'Maaf, Nama lokasi patroli harus diisi terlebih dahulu'
            ];
            echo json_encode($result);
        } elseif ($gps == '') {
            $result = [
                'status'    => FALSE,
                'pesan'   => 'Maaf, jarak lokasi harus diisi terlebih dahulu'
            ];
            echo json_encode($result);
        } else {
            $cekuid = $this->m_patroli->cekeksisuid($uid)->row();
            $jmlcekuid = $cekuid->jmluid;
            if (((int)$jmlcekuid) > 0) {
                $result = [
                    'status'    => FALSE,
                    'pesan'   => 'Maaf, UID yang anda masukkan sudah ada sebelumnya silahkan cek kembali'
                ];
                echo json_encode($result);
            } else {
                $data = [
                    'uid' => $uid,
                    'nama_lokasi' => $nama,
                    'lokasi_gps' => $gps,
                ];
                try {
                    $this->m_patroli->simpanlokasipatroli($data);
                    $result = [
                        'status'    => TRUE,
                        'pesan'   => 'Sukses simpan lokasi patroli'
                    ];
                    echo json_encode($result);
                } catch (Exception $err) {
                    $result = [
                        'status'    => FALSE,
                        'pesan'   => 'Gagal simpan lokasi patroli ' . $err
                    ];
                    echo json_encode($result);
                }
            }
        }
    }

    public function getlokasilistall_post()
    {
        $list = $this->m_patroli->getalldatalokasi()->result();
        $jmllist = count((array)$list);
        if ($jmllist > 0) {
            $result = [
                'status'    => TRUE,
                'pesan' => 'Data ditemukan',
                'data'   => $list
            ];
            echo json_encode($result);
        } else {
            $result = [
                'status'    => FALSE,
                'pesan'   => 'Maaf, data tidak ada',
                'data'    => ''
            ];
            echo json_encode($result);
        }
    }

    public function searchgetlokasilist_post()
    {
        $nama = $this->post('nama');
        $list = $this->m_patroli->getalldatalokasibynama($nama)->result();
        if ($nama == '') {
            $result = [
                'status'    => FALSE,
                'pesan' => 'Maaf, nama lokasi harus diisi terlebih dahulu',
                'data'   => ''
            ];
            echo json_encode($result);
        } else {
            $jmllist = count((array)$list);
            if ($jmllist > 0) {
                $result = [
                    'status'    => TRUE,
                    'pesan' => 'Data ditemukan',
                    'data'   => $list
                ];
                echo json_encode($result);
            } else {
                $result = [
                    'status'    => FALSE,
                    'pesan'   => 'Maaf, data yang anda cari tidak ditemukan',
                    'data'    => ''
                ];
                echo json_encode($result);
            }
        }
    }

    public function updatelokasipatroli_put()
    {
        $id_lokasi = $this->put('id_lokasi');
        $uid = $this->put('uid');
        $nama = $this->put('nama');
        $list = $this->m_patroli->getalldatalokasibyidlokasi($id_lokasi)->row();
        $jmllist = count((array)$list);
        if ($jmllist > 0) {
            if ($uid == '' and $nama == '') {
                $dataupdate = [
                    'uid' => $list->uid,
                    'nama_lokasi' => $list->nama_lokasi
                ];
                $this->m_patroli->updatenamalokasipatroli($dataupdate, $id_lokasi);
                $result = [
                    'status'    => TRUE,
                    'pesan' => 'Berhasil melakukan update nama lokasi',
                    'data'   => $dataupdate
                ];
                echo json_encode($result);
            } elseif ($uid == '') {
                $dataupdate = [
                    'uid' => $list->uid,
                    'nama_lokasi' => $nama
                ];
                $this->m_patroli->updatenamalokasipatroli($dataupdate, $id_lokasi);
                $result = [
                    'status'    => TRUE,
                    'pesan' => 'Berhasil melakukan update nama lokasi',
                    'data'   => $dataupdate
                ];
                echo json_encode($result);
            } elseif ($nama == '') {
                $dataupdate = [
                    'uid' => $uid,
                    'nama_lokasi' => $list->nama_lokasi
                ];
                $this->m_patroli->updatenamalokasipatroli($dataupdate, $id_lokasi);
                $result = [
                    'status'    => TRUE,
                    'pesan' => 'Berhasil melakukan update nama lokasi',
                    'data'   => $dataupdate
                ];
                echo json_encode($result);
            } else {
                $dataupdate = [
                    'uid' => $uid,
                    'nama_lokasi' => $nama
                ];
                $this->m_patroli->updatenamalokasipatroli($dataupdate, $id_lokasi);
                $result = [
                    'status'    => TRUE,
                    'pesan' => 'Berhasil melakukan update nama lokasi',
                    'data'   => $dataupdate
                ];
                echo json_encode($result);
            }
        } else {
            $result = [
                'status'    => FALSE,
                'pesan'   => 'Maaf, data tersebut tidak ditemukan silahkan cek kembali',
                'data'    => ''
            ];
            echo json_encode($result);
        }
    }

    public function hapuslokasipatroli_delete()
    {
        $id_lokasi = $this->delete('id_lokasi');
        if ($id_lokasi == '') {
            $result = [
                'status'    => FALSE,
                'pesan'   => 'Gagal, silahkan pilih lokasi yang mau dihapus',
            ];
            echo json_encode($result);
        } else {
            $list = $this->m_patroli->getalldatalokasibyidlokasi($id_lokasi)->row();
            $jmllist = count((array)$list);
            if ($jmllist > 0) {
                try {
                    $this->m_patroli->hapusnamalokasipatroli($id_lokasi);
                    $result = [
                        'status'    => TRUE,
                        'pesan' => 'Berhasil melakukan hapus nama lokasi',
                    ];
                    echo json_encode($result);
                } catch (Exception $err) {
                    $result = [
                        'status'    => FALSE,
                        'pesan' => 'Gagal melakukan hapus nama lokasi ' . $err,
                    ];
                    echo json_encode($result);
                }
            } else {
                $result = [
                    'status'    => FALSE,
                    'pesan'   => 'Maaf, data tersebut tidak ditemukan silahkan cek kembali',
                ];
                echo json_encode($result);
            }
        }
    }
}
